#ifndef PJSIP_H
#define PJSIP_H

#include <stationmgr.h>
#include <pjsua-lib/pjsua.h>
#include "webrtcadptr.h"

class PjSipadptr : public StationMgr
{
private:
    PjSipadptr();
    ~PjSipadptr();
    static PjSipadptr * p_instance;
    PjSipadptr(PjSipadptr const&);
    PjSipadptr& operator= (PjSipadptr const&);
    int accId;

public:
    static PjSipadptr * getInstance();

    int LoginOnStation(const QString &SipDomain, const QString &Server, const int &Port, const QString &UserName, const QString &Password);
    void Logout();
    int MakeCall(const QString &url);
    void AnswerCall(int call_id);
    void EndCall(int call_id);
    void EndMyCall(int call_id);
    void audioStreamCreate();
    QStringList enumInputDevice();
    QStringList enumOutputDevice();
    QString getIp();
    int getPort();


protected:
    static void incomingCall(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata);
    static void on_call_state(pjsua_call_id call_id, pjsip_event *e);
    static void onRegState(pjsua_acc_id acc_id, pjsua_reg_info *info);
    static void on_call_sdp_created(pjsua_call_id call_id, pjmedia_sdp_session *sdp, pj_pool_t *pool, const pjmedia_sdp_session *rem_sdp);
    static void on_stream_created(pjsua_call_id call_id, pjmedia_stream *strm, unsigned stream_idx, pjmedia_port **p_port);

    static void on_create_media_transport_srtp (pjsua_call_id call_id, unsigned media_idx, pjmedia_srtp_setting *srtp_opt);
    static void on_typing2(pjsua_call_id call_id, const pj_str_t *from, const pj_str_t *to, const pj_str_t *contact, pj_bool_t is_typing, pjsip_rx_data *rdata, pjsua_acc_id acc_id);
    static void on_call_rx_offer(pjsua_call_id call_id, const pjmedia_sdp_session *offer, void *reserved, pjsip_status_code *code, pjsua_call_setting *opt);
protected:
    pjsua_config *cfg;
    pjsua_logging_config *log_cfg;
    pjsua_transport_config *trp_cfg;
    pjsua_acc_config *acc_cfg;
    pjsip_transport *tp;
    pjsua_transport_id transport_id;
    QString thisIp;
    int Port;
};

#endif // PJSIP_H
