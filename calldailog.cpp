#include "calldailog.h"
#include "ui_calldailog.h"
#include "pjsipadptr.h"
#include "mainwindow.h"
#include <QApplication>
#include "webrtcadptr.h"

//#include <pjmedia_audiodev.h>


CallDailog::CallDailog(int id, StationMgr *mgr, /*WebRTCAdptr *adptr,*/ QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CallDailog),
    idCall(id),
    m_pMgr(mgr),
    wAdptr(new WebRTCAdptr)


{
    ui->setupUi(this);
    connect(m_pMgr, SIGNAL(CallState(QString, int)),this, SLOT(setCallState(QString, int)));
    //connect(m_pMgr, SIGNAL(SDP(QString, int)), wAdptr , SLOT(createUdpSocket(QString,int)));
    //connect(m_pMgr, SIGNAL(SDP(QString, int)), wAdptr , SLOT(createUdpRTCPSocket(QString,int)));
    QString ip = m_pMgr->getIp();
    int port = m_pMgr->getPort();
    wAdptr->createUdpSocket(ip, port);
    wAdptr->createUdpRTCPSocket(ip, port);
    //m_pMgr  = PjSipadptr::getInstance();
    //m_pMgr->audioStreamCreate();
    wAdptr->startAudioSession();
    //connect(m_pMgr, SIGNAL(SDP(QString,QString, int, int)), this, SLOT(setRtpParams(QString, QString, int, int)));
}

CallDailog::~CallDailog()
{
    delete ui;
}

void CallDailog::setCallInfo(const QString &name) {

    std::string checkName = name.toStdString();
    QRegularExpression reName("\"([^\"]+)\"");
    if((count(checkName.begin(), checkName.end(), '"'))==2)
    {
        QRegularExpressionMatch matchName = reName.match(name);
        QString name1    = matchName.captured(1);
        ui->Name->setText(name1);
    }
    else
    {
        ui->Name->hide();
    }

    QRegularExpression reNumber(":([^>]+)@");
    QRegularExpressionMatch matchNumber = reNumber.match(name);
    ui->Number->setText(matchNumber.captured(1));
}

void CallDailog::setCallState (const QString &name, int state) {
    setCallInfo(name);
    switch(state)
    {
    case 0:
        ui->dropButton->hide();
        ui->answerButton->hide();
        ui->declineButton->hide();
        break;
    case 1:
        ui->dropButton->show();
        ui->answerButton->hide();
        ui->declineButton->hide();
        break;
    case 2:
        ui->dropButton->hide();
        ui->answerButton->show();
        ui->declineButton->show();
        break;
    case 3:
        ui->dropButton->show();
        ui->answerButton->hide();
        ui->declineButton->hide();
        break;
    case 4:
        ui->dropButton->show();
        ui->answerButton->hide();
        ui->declineButton->hide();
        break;
    /*case 5:
        QWidget::close();
        break;*/
    case 6:
        QWidget::close();
        break;
    default: break;
    }
}

void CallDailog::on_answerButton_clicked()
{
    m_pMgr->AnswerCall(idCall);

   // m_pMgr->audioStreamCreate();
    //wAdptr->getInstance()->startAudioSession(myIp, remoteIp, mPort, remPort);

}

void CallDailog::on_dropButton_clicked()
{
    if(ui->Name->isHidden())
    {
       m_pMgr->EndMyCall(idCall);
    }
    else
    {
       m_pMgr->EndCall(idCall);
       QWidget::close();

    }

    deleteLater();
}

void CallDailog::on_declineButton_clicked() {
    on_answerButton_clicked();
    on_dropButton_clicked();
}

void CallDailog::closeEvent(QCloseEvent *)
{
    on_dropButton_clicked();
}

/*void CallDailog::setRtpParams(QString mIp, QString dIp, int myPort, int remotePort)
{
    myIp = mIp;
    remoteIp = dIp;
    mPort = myPort;
    remPort = remotePort;
}*/
