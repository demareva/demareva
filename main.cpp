#include "mainwindow.h"
#include <QApplication>
#include <pjsipadptr.h>
#include "settingsdialog.h"
#include "webrtcadptr.h"

#define PJMEDIA_HAS_LEGACY_SOUND_API 0
#define PJMEDIA_AUDIO_DEV_HAS_LEGACY_DEVICE 1

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Mera");
    QCoreApplication::setOrganizationDomain("mera.ru");
    QCoreApplication::setApplicationName("SimplePhone");
    MainWindow w;
    w.show();
    //WebRTCAdptr *wr = new WebRTCAdptr();
    return a.exec();
}
