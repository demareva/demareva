#include "contactmodel.h"

ContactModel *ContactModel::c_instance = nullptr;

ContactModel::ContactModel(QObject *parent) :
        QSqlTableModel(parent, QSqlDatabase::addDatabase("QSQLITE"))
{
    database().setDatabaseName( "ContactList" );
    database().open();

    QSqlQuery query;
    query.exec ("CREATE TABLE ContactList ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `name` TEXT, `number` TEXT )");
    setTable("ContactList");
}

void ContactModel::addContact(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO ContactList(id, name, number) "
                          "VALUES (:id, :name, :number)");
    if(data.size()!=0)
    {
        if(!((data[0].toString().isEmpty())&&(data[1].toString().isEmpty())))
        {
            if(data.size()<2)
            {
                query.bindValue(":name", data[0].toString());
            }
            else
            {
                if(data[0].toString().isEmpty())
                {
                    query.bindValue(":name", data[1].toString());
                }
                else
                {
                    query.bindValue(":name", data[0].toString());
                }
            query.bindValue(":number", data[1].toString());
            }
                query.exec();
                select();
        }
    }
}

void ContactModel::updateContact(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("UPDATE  ContactList SET name = :name, number = :number WHERE id = :id");
    if(data[1].toString().isEmpty())
    {
        query.bindValue(":name", data[2].toString());
    }
    else
    {
        query.bindValue(":name", data[1].toString());
    }
    query.bindValue(":number", data[2].toString());
    query.bindValue(":id", data[0].toString());
    query.exec();
    select();
}

ContactModel* ContactModel::getInstance()
{
    if(!ContactModel::c_instance)
                ContactModel::c_instance = new ContactModel();
            return ContactModel::c_instance;
}
