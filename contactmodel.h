#ifndef SQLMODEL_H
#define SQLMODEL_H

#include <QObject>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QSqlTableModel>


class ContactModel : public QSqlTableModel
{
    Q_OBJECT
public:

    void addContact(const QVariantList &data);
    void updateContact(const QVariantList &data);
    static ContactModel * getInstance();

signals:

public slots:

private:
    ContactModel(QObject *parent = Q_NULLPTR);
    //~ContactModel();
    static ContactModel * c_instance;

};

#endif // SQLMODEL_H
