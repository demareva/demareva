
#include <pjsipadptr.h>
#include "calldailog.h"
#include "settingsdialog.h"
#include <pjmedia_audiodev.h>
#include <string>
#include <QRegularExpression>
#include <QApplication>
#include <QStringList>
#include <stdio.h>
#include <pjmedia/audiodev.h>
#include <Audioclient.h>
#include <Audiopolicy.h>



PjSipadptr *PjSipadptr::p_instance = nullptr;


PjSipadptr::PjSipadptr() :
        accId(-1),
        cfg(new pjsua_config),
        log_cfg(new pjsua_logging_config),
        trp_cfg(new pjsua_transport_config),
        acc_cfg(new pjsua_acc_config),
        tp(new pjsip_transport),
        transport_id (-1) {

    pjsua_create();
    pjsua_config_default(cfg);
    cfg->cb.on_incoming_call = &incomingCall;
    cfg->cb.on_call_state = &on_call_state;
    cfg->cb.on_reg_state2 = &onRegState;
    cfg->cb.on_call_sdp_created = &on_call_sdp_created;
    cfg->cb.on_stream_created = &on_stream_created;
    cfg->cb.on_create_media_transport_srtp = &on_create_media_transport_srtp;
    cfg->cb.on_typing2= &on_typing2;
    cfg->cb.on_call_rx_offer = &on_call_rx_offer;
    pjsua_logging_config_default(log_cfg);
    log_cfg->console_level = 4;

    pjsua_init(cfg, log_cfg, NULL);
    pjsua_start();


}

PjSipadptr::~PjSipadptr()
{
    delete cfg;
    delete log_cfg;
    delete trp_cfg;
    delete acc_cfg;
    pjsua_destroy();
}
PjSipadptr* PjSipadptr::getInstance()
{
    if(!PjSipadptr::p_instance)
                PjSipadptr::p_instance = new PjSipadptr();
            return PjSipadptr::p_instance;
}

int PjSipadptr::LoginOnStation(const QString &SipDomain, const QString &Server, const int &Port, const QString& UserName, const QString& Password)
{
    int result;
    pjsua_acc_id acc_id;

    pjsua_transport_config_default(trp_cfg);
    trp_cfg->port = Port;

    result = pjsua_transport_create(PJSIP_TRANSPORT_TCP, trp_cfg, &transport_id);//Udp Null
    if (result == PJ_SUCCESS) {
    pjsua_set_null_snd_dev();

        std::string id = "sip:" + UserName.toStdString() + "@" + SipDomain.toStdString();
        std::string reg_uri = "sip:" + Server.toStdString() + ";transport=tcp;hide";
        std::string realm = SipDomain.toStdString();
        std::string scheme = "digest";
        std::string username = UserName.toStdString();
        std::string data = Password.toStdString();

        pjsua_acc_config_default(acc_cfg);

        acc_cfg->id = pj_str((char *)id.c_str());
        acc_cfg->reg_uri = pj_str((char *)reg_uri.c_str());
        acc_cfg->cred_count = 1;
        acc_cfg->cred_info[0].realm = pj_str((char *)realm.c_str());
        acc_cfg->cred_info[0].scheme = pj_str((char *)scheme.c_str());
        acc_cfg->cred_info[0].username = pj_str((char *)username.c_str());;
        acc_cfg->cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
        acc_cfg->cred_info[0].data = pj_str((char *)data.c_str());
        result = pjsua_acc_add(acc_cfg, PJ_TRUE, &acc_id);

        accId = acc_id;
    }

    return result;
}

void PjSipadptr::Logout()
{
    pjsua_transport_close(transport_id, true);
    pjsua_acc_set_registration(accId, 0);

    accId=-1;
}

int PjSipadptr::MakeCall(const QString& url) {

    int callId;
    std::string URL = url.toStdString();
    QRegularExpression re1(":([\\w\\W]+)@");
    QRegularExpression re2(":([\\w\\W]+)");
    QRegularExpression re3("([\\w\\W]+)@");
    QString resultURL;

    if((count(URL.begin(), URL.end(), ':'))&&(count(URL.begin(), URL.end(), '@')))
    {
      QRegularExpressionMatch match1 = re1.match(url);
      resultURL = match1.captured(1);
    }
    else if(count(URL.begin(), URL.end(), ':'))
    {
        QRegularExpressionMatch match2 = re2.match(url);
        resultURL = match2.captured(1);
    }
    else if(count(URL.begin(), URL.end(), '@'))
    {
        QRegularExpressionMatch match3 = re3.match(url);
        resultURL = match3.captured(1);
    }
    else
    {
       resultURL = url;
    }

    QSettings settings;
    QString server = settings.value("server.addr").toString();
    std::string URI = "sip:" + resultURL.toStdString() + "@" + server.toStdString() + ";transport=tcp;hide";

    pjsua_call_make_call(accId, &pj_str((char *)URI.c_str()),0, NULL, NULL, &callId);
    return callId;
}

void PjSipadptr::AnswerCall(int call_id) {
    pjsua_call_answer(call_id, 200, NULL, NULL);
}
void PjSipadptr::EndCall(int call_id) {
    pjsua_call_hangup(call_id, 0, NULL, NULL);
}

void PjSipadptr::EndMyCall(int call_id) {
    pjsua_call_hangup(call_id, 200, NULL, NULL);
}

void PjSipadptr::incomingCall(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata)
{

    pjsua_call_info ci;
    pjsua_call_get_info(call_id, &ci);
    QString name = QString::fromStdString(std::string(ci.remote_info.ptr, ci.remote_info.slen));
    PjSipadptr::getInstance()->IncomingCall(call_id, name, ci.state);
    pjsua_call_answer(call_id, 180, NULL, NULL);
}

void PjSipadptr::on_call_state(pjsua_call_id call_id, pjsip_event *e)
{

    pjsua_call_info ci;
    pjsua_call_get_info(call_id, &ci);
    QString name = QString::fromStdString(std::string(ci.remote_info.ptr, ci.remote_info.slen));

    PjSipadptr::getInstance()->CallState(name, ci.state);
}

QStringList PjSipadptr::enumInputDevice()
{
    QStringList deviceNames;
    pj_pool_factory *pf = new pj_pool_factory;
    pjmedia_aud_subsys_init(pf);
    int dev_count;
    pjmedia_aud_dev_index dev_idx;
    dev_count = pjmedia_aud_dev_count();
    for (dev_idx = 0; dev_idx < dev_count; ++dev_idx) {
        pjmedia_aud_dev_info info;
        int status = pjmedia_aud_dev_get_info(dev_idx, &info);
        if (info.input_count)
        deviceNames.append(info.name);
    }
    return deviceNames;
}

QStringList PjSipadptr::enumOutputDevice()
{
    QStringList deviceNames;
    pj_pool_factory *pf = new pj_pool_factory;
    pjmedia_aud_subsys_init(pf);
    int dev_count;
    pjmedia_aud_dev_index dev_idx;
    dev_count = pjmedia_aud_dev_count();
    for (dev_idx = 0; dev_idx < dev_count; ++dev_idx) {
        pjmedia_aud_dev_info info;
        int status = pjmedia_aud_dev_get_info(dev_idx, &info);
        if (info.output_count)
        deviceNames.append(info.name);
    }
    return deviceNames;
}

pj_status_t rec_cb(void *user_data, pjmedia_frame *frame) {
    return PJ_SUCCESS;
}
#include <QDebug>
pj_status_t play_cb (void *user_data, pjmedia_frame *frame) {
    QByteArray buff((const char *)frame->buf, frame->size);
    qDebug() << buff;
    return PJ_SUCCESS;
}

void PjSipadptr::audioStreamCreate()
{

    pjmedia_aud_dev_index inIdx = 0;
    pjmedia_aud_dev_index outIdx = 0;
    pj_pool_factory *pf = new pj_pool_factory;

    QSettings settings;
    pjmedia_aud_subsys_init(pf);
    int dev_count;
    pjmedia_aud_dev_index dev_idx;
    dev_count = pjmedia_aud_dev_count();
    for (dev_idx = 0; dev_idx < dev_count; ++dev_idx)
    {
        pjmedia_aud_dev_info info;
        int status = pjmedia_aud_dev_get_info(dev_idx, &info);
        if(info.name==settings.value("audio.input").toString())
        {
            inIdx = dev_idx;
        }
        if(info.name==settings.value("audio.output").toString())
        {
            outIdx = dev_idx;
        }
    }
    char buff[PJ_ERR_MSG_SIZE];
    //pj_strerror(pjsua_set_snd_dev(inIdx, outIdx), buff, PJ_ERR_MSG_SIZE);
    int val;
     pjmedia_aud_param *param1;
     pjmedia_aud_param *param2;
    //int status =pjmedia_aud_dev_default_param (inIdx, param1);
    //int status2 =pjmedia_aud_dev_default_param (outIdx, param2);
    //pjmedia_get_aud_subsys();
    pjmedia_aud_param param;
    //pj_strerror(pjsua_snd_get_setting(PJMEDIA_AUD_DEV_CAP_OUTPUT_VOLUME_SETTING, &val), buff, PJ_ERR_MSG_SIZE);
    //pj_strerror(pjsua_snd_get_setting(PJMEDIA_AUD_DEV_CAP_INPUT_VOLUME_SETTING, &val), buff, PJ_ERR_MSG_SIZE);
    param.dir = PJMEDIA_DIR_CAPTURE_PLAYBACK ;
    //param.rec_id = inIdx;
    //param.play_id = outIdx;
   param.clock_rate = 8000;
    param.channel_count = 1;
    param.samples_per_frame = 160;
    param.bits_per_sample = 16;
    pjmedia_aud_stream *stream;
    int status1;
    status1 = pjmedia_aud_stream_create(&param, &rec_cb/*NULL*/, &play_cb, NULL, &stream);
    pj_strerror(status1, buff, PJ_ERR_MSG_SIZE);

    //pjmedia_aud_stream_start(stream);
}

void PjSipadptr::onRegState(pjsua_acc_id acc_id, pjsua_reg_info *info)
{

    QSettings settings;
    QString user =  settings.value("user.name").toString();
    bool result = info->renew;
    PjSipadptr::getInstance()->RegStateChanged(result, user);
}
void PjSipadptr::on_call_sdp_created(pjsua_call_id call_id, pjmedia_sdp_session *sdp, pj_pool_t *pool, const pjmedia_sdp_session *rem_sdp)
{
    /*QString remoteIp = rem_sdp->origin.addr.ptr;
    int remotePort = rem_sdp->media[0]->desc.port;*/
    QString myIp = sdp->origin.addr.ptr;
    int myPort = sdp->media[0]->desc.port;
    QRegularExpression Ip("^[\\d.]+");
    //QString dIp;
    QString mIp;
    QRegularExpressionMatch matchMyIp = Ip.match(myIp);
    mIp = matchMyIp.captured(0);
    //QRegularExpressionMatch matchRemIp = Ip.match(remoteIp);
    //dIp = matchRemIp.captured(0);
    //PjSipadptr::getInstance()->SDP(mIp, myPort);
    PjSipadptr::getInstance()->thisIp=mIp;
    PjSipadptr::getInstance()->Port = myPort;
}

void PjSipadptr::on_stream_created(pjsua_call_id call_id, pjmedia_stream *strm, unsigned stream_idx, pjmedia_port **p_port) {

}

void PjSipadptr::on_create_media_transport_srtp (pjsua_call_id call_id, unsigned media_idx, pjmedia_srtp_setting *srtp_opt)
{

}
void PjSipadptr::on_typing2(pjsua_call_id call_id, const pj_str_t *from, const pj_str_t *to, const pj_str_t *contact, pj_bool_t is_typing, pjsip_rx_data *rdata, pjsua_acc_id acc_id)
{

}
void PjSipadptr::on_call_rx_offer(pjsua_call_id call_id, const pjmedia_sdp_session *offer, void *reserved, pjsip_status_code *code, pjsua_call_setting *opt)
{

}

QString PjSipadptr::getIp()
{
    return thisIp;
}
int PjSipadptr::getPort()
{
    return Port;
}
