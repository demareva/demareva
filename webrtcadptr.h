#ifndef WEBRTCADPTR_H
#define WEBRTCADPTR_H
#include <QObject>
#include <voe_base.h>
#include <voe_hardware.h>
#include <voe_file.h>
#include <QtNetwork>
#include <QUdpSocket>
#include "stationmgr.h"
#include "pjsipadptr.h"
#include <voe_network.h>
#include <voe_volume_control.h>
using namespace webrtc;

//class StationMgr;

class WebRTCAdptr: public QObject, public Transport
{
    Q_OBJECT
public:
    //static WebRTCAdptr * getInstance();
    WebRTCAdptr();
    static QStringList EnumPlayoutDevices();
    static QStringList EnumRecordingDevices();
    void SetPlayoutDevice();
    void SetRecordingDevice();
    //void createUdpSocket(QString mIp, int myPort);
    int SendPacket(int channel, const void *data, int len) override;
    int SendRTCPPacket(int channel, const void *data, int len) override;
    void createUdpSocket(QString mIp, int myPort);
    void createUdpRTCPSocket(QString mIp, int myPort);

public slots:
    void startAudioSession();
    void readDatagrams();
    void readRTCPDatagrams();
    //void createUdpSocket(QString mIp, int myPort);
    //void createUdpRTCPSocket(QString mIp, int myPort);
    void setSocketParams(QString mIp, int myPort);

private:
    //static WebRTCAdptr * w_instance;
    QString Ip;
    int Port;

    int channel;

protected:
    VoiceEngine* voe;
    VoEBase* base;
    VoEHardware* hardware;
    VoEFile* voeFile;
    VoENetwork* netw;
    VoiceEngineObserver* observer;
    VoEVolumeControl* volume;
    QUdpSocket* udpSocket;
    QUdpSocket* udpRTCPSocket;

};

#endif // WEBRTCADPTR_H
