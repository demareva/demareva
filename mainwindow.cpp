#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "calldailog.h"
#include "settingsdialog.h"
#include "contactlistdialog.h"
#include "pjsipadptr.h"
#include "contactmodel.h"
#include <QKeyEvent>
#include <QMenu>
#include <QRegularExpression>
#include "webrtcadptr.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    registrationState(false)

{
    ui->setupUi(this);
    ui->labelUser->hide();
    ui->makeButton->setDisabled(true);
    ui->addButton->hide();

    m_pMgr  = PjSipadptr::getInstance();

    contactModel = ContactModel::getInstance();
    contactModel->setSort(1,Qt::AscendingOrder);
    proxy = new QSortFilterProxyModel();

    connect(m_pMgr, SIGNAL(IncomingCall(int, QString, int)), this, SLOT(incCall(int, QString, int)));
    connect(m_pMgr, SIGNAL(RegStateChanged(bool, QString)), this, SLOT(regStChanged (bool, QString)));

    //connect(m_pMgr, SIGNAL(SDP(QString, int)), wAdptr , SLOT(createUdpSocket(QString,int)));
    //connect(m_pMgr, SIGNAL(SDP(QString, int)), wAdptr , SLOT(createUdpRTCPSocket(QString,int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::incCall(const int call_id, const QString &name, int state)
{
    CallDailog* cDial = new CallDailog(call_id, m_pMgr, /*wAdptr,*/ this);
    cDial->setCallState(name, state);
    cDial->show();
}

void MainWindow::regStChanged (bool result, const QString &user)
{
    if (result==1)
    {
       ui->labelUser->setText(user);
       ui->labelUser->show();
       ui->makeButton->setDisabled(false);
       registrationState = true;
    }
    else
    {
        ui->labelUser->hide();
        ui->makeButton->setDisabled(true);
        registrationState = false;
    }
}

void MainWindow::on_settingsButton_clicked() {

    SettingsDialog dlg(m_pMgr/*, wAdptr*/, this);
    connect(&dlg, SIGNAL(save()), this, SLOT(saveSettings()));
    dlg.exec();
}


void MainWindow::on_makeButton_clicked()
{
    QString URL = ui->lineEditUrl->text();
    QRegularExpression re1("[\\D]");
    if(!URL.contains(re1))
    {
        int id  = m_pMgr->MakeCall(ui->lineEditUrl->text());
        CallDailog* cDial = new CallDailog(id, m_pMgr/*, wAdptr*/);
        cDial->show();
        ui->lineEditUrl->clear();
    }
    else
    {
                QModelIndexList match;
                QModelIndex firstMatch;
                for(int i = 0; i < contactModel->rowCount(); i++ )
                {
                    match = contactModel->match(contactModel->index(i,1),Qt::DisplayRole,URL);
                }
                firstMatch=match.value(0);
                //QString un = contactModel->data(contactModel->index(firstMatch.row(),2)).toString();
                int id  = m_pMgr->MakeCall(contactModel->data(contactModel->index(firstMatch.row(),2)).toString());
                CallDailog* cDial = new CallDailog(id, m_pMgr/*, wAdptr*/);
                cDial->show();

    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key()){
       case Qt::Key_Return:
            if(registrationState==true)
            on_makeButton_clicked();
            break;

        case Qt::Key_Enter:
            if(registrationState==true)
            on_makeButton_clicked();
            break;
      }
}


void MainWindow::on_addButton_clicked()
{
    ContactListDialog contactDlg(this);
    connect(&contactDlg, SIGNAL(addContact(const QVariantList&)), this, SLOT(addToContactList(const QVariantList&)));
    contactDlg.exec();
}

void MainWindow::addToContactList(const QVariantList& data)
{
    contactModel->addContact(data);
    ui->listContact->reset();
}

void MainWindow::editContactList(const QVariantList& data)
{
    contactModel->updateContact(data);
    ui->listContact->reset();
}

void MainWindow::on_listContact_customContextMenuRequested(const QPoint &pos)
{

    QMenu * menu = new QMenu(this);

    QAction * callToC = new QAction(("Call"), this);
    QAction * editC = new QAction(("Edit"), this);
    QAction * deleteC = new QAction(("Delete"), this);
    connect(deleteC, SIGNAL(triggered()), this, SLOT(deleteContact()));
    connect(callToC, SIGNAL(triggered()), this, SLOT(makeCallToContact()));
    connect(editC, SIGNAL(triggered()), this, SLOT(editContact()));
    menu->addAction(callToC);
    menu->addSeparator();
    menu->addAction(editC);
    menu->addAction(deleteC);
    if(registrationState==false)
    {
        callToC->setDisabled(true);
    }
    menu->popup(ui->listContact->viewport()->mapToGlobal(pos));
}

void MainWindow::deleteContact()
{
    QModelIndex origin  = proxy->mapToSource(ui->listContact->selectionModel()->currentIndex());
    int row = origin.row();

    if(row >= 0)
    {
        contactModel->removeRow(row);
        contactModel->select();
    }

}

void MainWindow::makeCallToContact()
{
    QModelIndex origin  = proxy->mapToSource(ui->listContact->selectionModel()->currentIndex());
    int row = origin.row();

    {
       int id  = m_pMgr->MakeCall(contactModel->data(contactModel->index(row, 2)).toString());
       CallDailog* cDial = new CallDailog(id, m_pMgr/*, wAdptr*/);
       cDial->show();
    }
}

void MainWindow::editContact()
{
    QModelIndex origin  = proxy->mapToSource(ui->listContact->selectionModel()->currentIndex());
    int row = origin.row();

    if(row >= 0)
    {
        QString name = contactModel->data(contactModel->index(row, 1)).toString();
        QString number = contactModel->data(contactModel->index(row, 2)).toString();
        int id = contactModel->data(contactModel->index(row, 0)).toInt();
        QVariantList data;
        data.append(id);
        data.append(name);
        data.append(number);
        ContactListDialog contactDlg(this);
        connect(this, SIGNAL(editThContact(const QVariantList&)), &contactDlg, SLOT(editThisContact(const QVariantList&)));
        connect(&contactDlg, SIGNAL(contactEdit(const QVariantList&)), this, SLOT(editContactList(const QVariantList&)));
        emit editThContact(data);
        contactDlg.exec();
    }
}

void MainWindow::on_lineEditUrl_textChanged(const QString &arg1)
{
    QString reg = "^"+arg1;
    proxy->setFilterRegExp(reg);
    proxy->setFilterKeyColumn(1);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    ui->listContact->setModel(proxy);
}

void MainWindow::on_contactButton_clicked()
{
    ui->addButton->show();
    proxy->setSourceModel(contactModel);
    ui->listContact->setModel(proxy);
    ui->listContact->setModelColumn(1);
    ui->listContact->setEditTriggers(QAbstractItemView::NoEditTriggers);
    contactModel->select();
    ui->listContact->setContextMenuPolicy(Qt::CustomContextMenu);
}

void MainWindow::saveSettings()
{
    if((ui->State->currentIndex()==1)&&(registrationState==true))
    {
        m_pMgr->Logout();
        QSettings settings;

        QString addr    = settings.value("server.addr").toString();
        QString port = settings.value("server.port").toString();
        QString domain = settings.value("domain").toString();
        QString user =  settings.value("user.name").toString();
        QString password = settings.value("user.password").toString();
        if(addr.isEmpty()||port.isEmpty()||domain.isEmpty()||user.isEmpty()||password.isEmpty())
        {
            on_settingsButton_clicked();
        }
        else
        {
            char buff[PJ_ERR_MSG_SIZE];
            int result  = m_pMgr->LoginOnStation(domain, addr, port.toInt(), user, password);

            pj_strerror(result, buff, PJ_ERR_MSG_SIZE);
            ui->statusBar->showMessage(buff);
        }
    }
    else if((ui->State->currentIndex()==1)&&(registrationState==false))
    {
        QSettings settings;

        QString addr    = settings.value("server.addr").toString();
        QString port = settings.value("server.port").toString();
        QString domain = settings.value("domain").toString();
        QString user =  settings.value("user.name").toString();
        QString password = settings.value("user.password").toString();
        if(addr.isEmpty()||port.isEmpty()||domain.isEmpty()||user.isEmpty()||password.isEmpty())
        {
            on_settingsButton_clicked();
        }
        else
        {
            char buff[PJ_ERR_MSG_SIZE];
            int result  = m_pMgr->LoginOnStation(domain, addr, port.toInt(), user, password);

            pj_strerror(result, buff, PJ_ERR_MSG_SIZE);
            ui->statusBar->showMessage(buff);
        }
    }
    else if((ui->State->currentIndex()==0)&&(registrationState==false))
    {}
}

void MainWindow::on_State_currentIndexChanged(int index)
{
    if(index==1)
    {
        QSettings settings;

        QString addr    = settings.value("server.addr").toString();
        QString port = settings.value("server.port").toString();
        QString domain = settings.value("domain").toString();
        QString user =  settings.value("user.name").toString();
        QString password = settings.value("user.password").toString();
        if(addr.isEmpty()||port.isEmpty()||domain.isEmpty()||user.isEmpty()||password.isEmpty())
        {
            on_settingsButton_clicked();
        }
        else
        {
            char buff[PJ_ERR_MSG_SIZE];
            int result  = m_pMgr->LoginOnStation(domain, addr, port.toInt(), user, password);

            pj_strerror(result, buff, PJ_ERR_MSG_SIZE);
            ui->statusBar->showMessage(buff);
        }
    }
    else if(index==0)
    {
        if(registrationState==true)
        m_pMgr->Logout();
    }
}

void MainWindow::on_pushButton_clicked()
{

}
