#include "settingsdialog.h"
#include "ui_settingsdialog.h"
//#include <pjmedia_audiodev.h>
#include "stationmgr.h"
#include "webrtcadptr.h"

SettingsDialog::SettingsDialog(StationMgr *mgr, /*WebRTCAdptr *adptr,*/ QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog),
    m_pMgr(mgr)
{
    ui->setupUi(this);
    ui->inputDevice->addItems(WebRTCAdptr::EnumRecordingDevices());
    ui->outputDevice->addItems(WebRTCAdptr::EnumPlayoutDevices());
    //ui->inputDevice->addItems(m_pMgr->enumInputDevice());
    //ui->outputDevice->addItems(m_pMgr->enumOutputDevice());
    readSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::writeSettings()
{
    QSettings settings;

    settings.setValue("server.addr", ui->lineEditServer->text());
    settings.setValue("server.port", ui->lineEditPort->text());
    settings.setValue("domain", ui->lineEditDomain->text());
    settings.setValue("user.name", ui->lineEditUser->text());
    settings.setValue("user.password", ui->lineEditPassword->text());
    settings.setValue("audio.input", ui->inputDevice->currentText());
    settings.setValue("audio.output", ui->outputDevice->currentText());
    settings.sync();
}

void SettingsDialog::readSettings()
{
    QSettings settings;

    ui->lineEditServer->setText(settings.value("server.addr", "192.168.61.216").toString());
    ui->lineEditPort->setText(settings.value("server.port", "5060").toString());
    ui->lineEditDomain->setText(settings.value("domain", "merann.ru").toString());
    ui->lineEditUser->setText(settings.value("user.name", "5016").toString());
    ui->lineEditPassword->setText(settings.value("user.password","123456").toString());
    ui->inputDevice->setCurrentText(settings.value("audio.input").toString());
    ui->outputDevice->setCurrentText(settings.value("audio.output").toString());
}

void SettingsDialog::on_saveButton_clicked()
{
    writeSettings();
    QWidget::close();
    emit save();
}

void SettingsDialog::on_cancelButton_clicked()
{
    QWidget::close();
}

