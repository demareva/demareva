#ifndef CALLDAILOG_H
#define CALLDAILOG_H

#include <QDialog>
//#include <pjmedia_audiodev.h>
#include "webrtcadptr.h"

class StationMgr;
class WebRtcAdptr;

namespace Ui {
class CallDailog;
}

class CallDailog : public QDialog
{
    Q_OBJECT
signals:

public:
    explicit CallDailog(int id, StationMgr *mgr/*, WebRTCAdptr *adptr*/, QWidget *parent = 0);
    ~CallDailog();
    void closeEvent(QCloseEvent *);


public slots:
   void setCallInfo(const QString &name);
   void setCallState(const QString &name, int state);
   //void setRtpParams(QString mIp, QString dIp, int mPort, int remPort);

private slots:
    void on_answerButton_clicked();

    void on_dropButton_clicked();

    void on_declineButton_clicked();

private:
    Ui::CallDailog *ui;
    StationMgr *m_pMgr;
    int idCall;
    //WebRTCAdptr *wAdptr;
    QString myIp;
    QString remoteIp;
    int mPort;
    int remPort;

protected:
    //pjmedia_aud_stream *stream;
    WebRTCAdptr *wAdptr;
};

#endif // CALLDAILOG_H
