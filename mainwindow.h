#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlTableModel>
#include "calldailog.h"
#include "contactlistdialog.h"
#include "contactmodel.h"
#include <QSortFilterProxyModel>
#include "webrtcadptr.h"

namespace Ui {
class MainWindow;
}

class StationMgr;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void editThContact(const QVariantList& data);

public slots:
    void incCall(const int call_id, const QString &name, int state);
    void keyPressEvent(QKeyEvent *event);
    void regStChanged (bool result, const QString& user);
    void addToContactList(const QVariantList &data);
    void editContactList(const QVariantList& data);
    void deleteContact();
    void makeCallToContact();
    void editContact();
    void saveSettings();

private slots:

    void on_settingsButton_clicked();

    void on_makeButton_clicked();

    void on_addButton_clicked();

    void on_listContact_customContextMenuRequested(const QPoint &pos);

    void on_lineEditUrl_textChanged(const QString &arg1);

    void on_contactButton_clicked();

    void on_State_currentIndexChanged(int index);

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;

    StationMgr *m_pMgr;
    ContactModel *contactModel;
    ContactListDialog *cd;
    QSortFilterProxyModel* proxy;
    bool registrationState;
    //WebRTCAdptr *wAdptr;

private:
     void setupModel(const QString &tableName, const QStringList &headers);
     void createUI();
};

#endif // MAINWINDOW_H
