#include <QtNetwork>
#include <QUdpSocket>
#include "webrtcadptr.h"
#include <voe_base.h>
#include <voe_hardware.h>
#include "settingsdialog.h"
//#include <channel.h>
//#include <voe_rtp_rtcp_impl.h>
#include <voe_file.h>
#include <QDebug>
#include "stationmgr.h"
#include "pjsipadptr.h"
#include <voe_network.h>
#include <voe_volume_control.h>
#include <voe_errors.h>


using namespace webrtc;

//WebRTCAdptr *WebRTCAdptr::w_instance = nullptr;

WebRTCAdptr::WebRTCAdptr()


{
    voe = VoiceEngine::Create();
    base = VoEBase::GetInterface(voe);
    hardware  = VoEHardware::GetInterface(voe);
    voeFile =VoEFile::GetInterface(voe);
    netw  = VoENetwork::GetInterface(voe);
    VoEVolumeControl* volume  = VoEVolumeControl::GetInterface(voe);
    base->Init();
    base->RegisterVoiceEngineObserver(*observer);

}

/*WebRTCAdptr* WebRTCAdptr::getInstance()
{
    if(!WebRTCAdptr::w_instance)
        WebRTCAdptr::w_instance = new WebRTCAdptr();
    return WebRTCAdptr::w_instance;
}*/

QStringList WebRTCAdptr::EnumPlayoutDevices()
{
    VoiceEngine* voe1 = VoiceEngine::Create();
    VoEBase* base1 = VoEBase::GetInterface(voe1);
    VoEHardware* hardware1  = VoEHardware::GetInterface(voe1);
    base1->Init();
    int devNumber;
    hardware1->GetNumOfPlayoutDevices(devNumber);
    QStringList playoutDeviceNames;

    char devName[128];
    char GUID [128];
    for(int i=0; i < devNumber; i++)
    {
        hardware1->GetPlayoutDeviceName(i, devName, GUID);
        playoutDeviceNames.append(devName);

    }
    return playoutDeviceNames;
}

QStringList WebRTCAdptr::EnumRecordingDevices()
{
    VoiceEngine* voe1 = VoiceEngine::Create();
    VoEBase* base1 = VoEBase::GetInterface(voe1);
    VoEHardware* hardware1  = VoEHardware::GetInterface(voe1);
    base1->Init();
    int devNumber;
    hardware1->GetNumOfRecordingDevices(devNumber);
    QStringList recordingDeviceNames;

    char devName[128];
    char GUID [128];
    for(int i=0; i < devNumber; i++)
    {
        hardware1->GetRecordingDeviceName(i, devName, GUID);
        recordingDeviceNames.append(devName);

    }
    return recordingDeviceNames;
}

void WebRTCAdptr::SetPlayoutDevice()
{
   QSettings settings;
   int devNumber;
   hardware->GetNumOfPlayoutDevices(devNumber);
   char devName[128];
   char GUID [128];
   int outIdx;
   for(int i=0; i < devNumber; i++)
   {
       hardware->GetPlayoutDeviceName(i, devName, GUID);
       if(devName == settings.value("audio.output").toString())
       {
           outIdx = i;
           break;
       }
   }
   int j = hardware->SetPlayoutDevice(outIdx);
}

void WebRTCAdptr::SetRecordingDevice()
{
   QSettings settings;
   int devNumber;
   hardware->GetNumOfRecordingDevices(devNumber);
   char devName[128];
   char GUID [128];
   int inIdx;
   for(int i=0; i < devNumber; i++)
   {
       hardware->GetRecordingDeviceName(i, devName, GUID);
       if(devName == settings.value("audio.input").toString())
       {
           inIdx = i;
           break;
       }
   }
   int j = hardware->SetRecordingDevice(inIdx);
}

void WebRTCAdptr::startAudioSession()
{
    this->SetPlayoutDevice();
    this->SetRecordingDevice();

    channel = base->CreateChannel();

    float scaling;

    //GetChannelOutputVolumeScaling(channel, scaling);

    //virtual int SetOutputVolumePan(int channel, float left, float right);
    //RegisterRTPObserver(channelId, VoERTPObserver& observer);

   /*base->SetLocalReceiver(channelId, myPort, myPort+1, myIp.c_str());

   base->SetSendDestination(channelId, remotePort, remoteIp.c_str());*/

    //base->SetVmonSendDestination(channelId, 1024, "192.168.127.31");

    int rc = netw->RegisterExternalTransport(channel, *this);

    rc = base->StartPlayout(channel);

    rc = base->StartReceive(channel);
    rc = base->LastError();



    //voeFile->StartPlayingFileLocally(channelId,"D:\\Projects\\first-dep\\telephone-ring-04.wav");
}

void WebRTCAdptr::setSocketParams(QString mIp, int myPort)
{
    Ip = mIp;
    Port = myPort;
}

void WebRTCAdptr::createUdpSocket(QString mIp, int myPort)
{
    udpSocket = new QUdpSocket();
    QHostAddress addr;
    addr.setAddress(mIp);
    bool result = udpSocket->bind(addr, myPort);
    connect(udpSocket, SIGNAL(readyRead()),this, SLOT(readDatagrams()));
}

void WebRTCAdptr::createUdpRTCPSocket(QString mIp, int myPort)
{
    udpRTCPSocket = new QUdpSocket();
    QHostAddress addr;
    addr.setAddress(mIp);
    bool result = udpSocket->bind(addr, myPort+1);
    connect(udpRTCPSocket, SIGNAL(readyRead()),this, SLOT(readRTCPDatagrams()));
}

void WebRTCAdptr::readDatagrams()
{

    while (udpSocket->hasPendingDatagrams()) {
        QByteArray buffer;
        buffer.resize(udpSocket->pendingDatagramSize());

        QHostAddress sender;
        quint16 senderPort;
        udpSocket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

        int rc = netw->ReceivedRTPPacket(channel, buffer.data(), buffer.size() );
        if(rc != 0) {
            qDebug() << base->LastError();
        }
    }
}

void WebRTCAdptr::readRTCPDatagrams()
{
    while (udpRTCPSocket->hasPendingDatagrams()) {
        QByteArray buffer;
        buffer.resize(udpRTCPSocket->pendingDatagramSize());

        QHostAddress sender;
        quint16 senderPort;
        udpRTCPSocket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

        int rc = netw->ReceivedRTCPPacket(channel, buffer.data(), buffer.size() );
        if(rc != 0) {
            qDebug() << base->LastError();
        }
    }
}

int WebRTCAdptr::SendPacket(int channel, const void *data, int len)
{
    return 0;
}
int WebRTCAdptr::SendRTCPPacket(int channel, const void *data, int len)
{
    return 0;
}
