#ifndef CONTACTLISTDIALOG_H
#define CONTACTLISTDIALOG_H

#include <QDialog>

namespace Ui {
class ContactListDialog;
}

class ContactListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ContactListDialog(QWidget *parent = 0);
    ~ContactListDialog();
signals:
  void addContact(const QVariantList& data);
  void contactEdit(const QVariantList& data);

public slots:

  void editThisContact(const QVariantList& data);

private slots:
    void on_addButton_clicked();

    void on_cancelButton_clicked();

    void on_updateButton_clicked();

private:
    Ui::ContactListDialog *ui;
    ContactListDialog *cld;
    int id;
};

#endif // CONTACTLISTDIALOG_H
