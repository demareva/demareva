#ifndef STATIONMGR_H
#define STATIONMGR_H

#include <QObject>

class StationMgr : public QObject
{
    Q_OBJECT

public:
    explicit StationMgr(QObject *parent = 0);
    virtual QString getIp() = 0;
    virtual int getPort() = 0;
signals:
    void IncomingCall(int call_id, const QString &info, const int &call_state);
    void CallState (const QString &info, const int &call_state);
    void RegStateChanged(bool result, const QString &user);
    void SDP(QString mIp, int myPort);

public slots:
    virtual int LoginOnStation(const QString &SipDomain, const QString &Server, const int &Port, const QString &UserName, const QString &Password) = 0;
    virtual void Logout() = 0;
    virtual int MakeCall(const QString& url) = 0;
    virtual void AnswerCall(int call_id) = 0;
    virtual void EndCall(int call_id) = 0;
    virtual void EndMyCall(int call_id) = 0;
    virtual void audioStreamCreate() = 0;
    virtual QStringList enumInputDevice() = 0;
    virtual QStringList enumOutputDevice() = 0;
};

#endif // STATIONMGR_H
