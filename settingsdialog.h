#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>

class StationMgr;
class WebRTCAdptr;

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(StationMgr *mgr, QWidget *parent = 0);
    ~SettingsDialog();
    void writeSettings();
    void readSettings();

signals:
    void save();

private slots:
    void on_saveButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::SettingsDialog *ui;
    StationMgr *m_pMgr;
   // WebRTCAdptr *wAdptr;
};

#endif // SETTINGSDIALOG_H
