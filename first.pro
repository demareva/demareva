#-------------------------------------------------
#
# Project created by QtCreator 2017-09-15T12:49:16
#
#-------------------------------------------------

QT       += core gui
QT       += sql
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = first
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    stationmgr.cpp \
    pjsipadptr.cpp \
    calldailog.cpp \
    settingsdialog.cpp \
    contactlistdialog.cpp \
    contactmodel.cpp \
    webrtcadptr.cpp

INCLUDEPATH += \
    ../first-dep/pjsip/pjsip/include \
    ../first-dep/pjsip/pjlib/include \
    ../first-dep/pjsip/pjlib-util/include \
    ../first-dep/pjsip/pjmedia/include \
    ../first-dep/pjsip/pjnath/include \
    ../first-dep/WebRTC/src \
    ../first-dep/WebRTC/src/voice_engine/include \
    ../first-dep/WebRTC/src/voice_engine \
    ../first-dep/WebRTC/src/system_wrappers/interface \
    ../first-dep/WebRTC/Debug


LIBS += \
    -L"../first-dep/pjsip/pjsip/lib" \
    -L"../first-dep/pjsip/pjlib/lib" \
    -L"../first-dep/pjsip/pjlib-util/lib" \
    -L"../first-dep/pjsip/pjmedia/lib" \
    -L"../first-dep/pjsip/pjnath/lib" \
    -L"../first-dep/pjsip/third_party/lib" \
    -L"../first-dep/WebRTC/Debug" \
    -lpjsip-core-x86_64-x64-vc14-Debug \
    -lpjsip-simple-x86_64-x64-vc14-Debug \
    -lpjsip-ua-x86_64-x64-vc14-Debug \
    -lpjsua2-lib-x86_64-x64-vc14-Debug \
    -lpjsua-lib-x86_64-x64-vc14-Debug \
    -lpjlib-x86_64-x64-vc14-Debug \
    -lpjlib-util-x86_64-x64-vc14-Debug \
    -lpjmedia-audiodev-x86_64-x64-vc14-Debug \
    -lpjmedia-codec-x86_64-x64-vc14-Debug \
    -lpjmedia-videodev-x86_64-x64-vc14-Debug \
    -lpjmedia-x86_64-x64-vc14-Debug \
    -lpjnath-x86_64-x64-vc14-Debug \
    -llibgsmcodec-x86_64-x64-vc14-Debug \
    -llibspeex-x86_64-x64-vc14-Debug \
    -llibsrtp-x86_64-x64-vc14-Debug \
    -llibilbccodec-x86_64-x64-vc14-Debug \
    -llibresample-x86_64-x64-vc14-Debug \
    -lWebRTCEngine \
    -lWs2_32 \
    -lOle32 \
    -lMsdmo \
    -lDmoguids \
    -lwmcodecdspuuid \
    -lamstrmid



DEFINES += PJ_WIN32=1

HEADERS  += mainwindow.h \
    stationmgr.h \
    pjsipadptr.h \
    calldailog.h \
    settingsdialog.h \
    contactlistdialog.h \
    contactmodel.h \
    webrtcadptr.h

FORMS    += mainwindow.ui \
    calldailog.ui \
    settingsdialog.ui \
    contactlistdialog.ui


