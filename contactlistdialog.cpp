#include "contactlistdialog.h"
#include "ui_contactlistdialog.h"

ContactListDialog::ContactListDialog(QWidget *parent) :
    QDialog(parent),
    id(-1),
    ui(new Ui::ContactListDialog)
{
    ui->setupUi(this);
    ui->updateButton->hide();
}

ContactListDialog::~ContactListDialog()
{
    delete ui;
}

void ContactListDialog::on_addButton_clicked()
{
    QVariantList data;
    data.append(ui->lineEditName->text());
    data.append(ui->lineEditNumber->text());
    emit addContact(data);
    QWidget::close();
}

void ContactListDialog::on_cancelButton_clicked()
{
    QWidget::close();
}

void ContactListDialog::editThisContact(const QVariantList& data)
{
    id = data[0].toInt();
    ui->lineEditName->setText(data[1].toString());
    ui->lineEditNumber->setText(data[2].toString());
    ui->addButton->hide();
    ui->updateButton->show();
}

void ContactListDialog::on_updateButton_clicked()
{
    QVariantList data;
    data.append(id);
    data.append(ui->lineEditName->text());
    data.append(ui->lineEditNumber->text());
    emit contactEdit(data);
    QWidget::close();
}
